var authController = require('../controllers/authcontroller.js');

module.exports = function(app,passport){

// index routes
	app.get('/', (req, res) => {
		res.render('logo');
    });

    app.get('/personalizar', (req, res) => {
		res.render('personalizar');
	});
	app.get('/productos', (req, res) => {
		res.render('productos');
	});
	app.get('/catalogo', (req, res) => {
		res.render('catalogo');
	});
	app.get('/login', (req, res) => {
		res.render('login.ejs');
	});
	//login view
	app.get('/login', (req, res) => {
		res.render('login.ejs', {
			message: req.flash('loginMessage')
		});
	});
    
    //profile view
app.get('/dashboard', isLoggedIn, (req, res) => {
    res.render('dashboard', {
        user: req.user
    });
});

    app.get('/signup', authController.signup);


app.get('/signin', authController.signin);


app.post('/signup', passport.authenticate('local-signup',  { successRedirect: '/dashboard',
                                                    failureRedirect: '/signup'}
                                                    ));


app.get('/dashboard',isLoggedIn, authController.dashboard);


app.get('/logout',authController.logout);


app.post('/signin', passport.authenticate('local-signin',  { successRedirect: '/dashboard',
                                                    failureRedirect: '/signin'}
                                                    ));


function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/signin');
}


}






